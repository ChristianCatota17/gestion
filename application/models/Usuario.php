<?php
  class Usuario extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    public function buscarUsuarioPorEmailPassword($email_usu,$password_usu){
      $this->db->where("email_usu",$email_usu);
      $this->db->where("password_usu",$password_usu);
      $usuarioEncontrado=$this->db->get("usuario");
      if ($usuarioEncontrado->num_rows()>0) {
        return $usuarioEncontrado->row();
      }else {//cuando las credenciales estan incorrectas
        return false;
      }
    }

    public function insertar($datos){
      return $this->db->insert('usuario',$datos);
    }

    public function consultarTodos(){
      $listadoUsuarios=$this->db->get('usuario');
      if ($listadoUsuarios->num_rows()>0) {
        return $listadoUsuarios;
      }else{
        return false;
      }
    }

    public function eliminar($id_usu){
      $this->db->where("id_usu",$id_usu);
      return $this->db->delete("usuario");
    }

    public function actualizar($data, $id_usu){
        $this->db->where("id_usu",$id_usu);
        return $this->db->update("usuario",$data);
    }

    public function consultarPorId($id_usu){
      $this->db->where('id_usu',$id_usu);
      $usuario=$this->db->get('usuario');
      if ($usuario->num_rows()>0) {
        return $usuario->row();
      }else{
        return false;
      }
    }

    public function obtenerPorEmail($email_usu){
      $this->db->where("email_usu",$email_usu);
      $usuario=$this->db->get("usuario");
      if($usuario->num_rows()>0){
        return $usuario->row();
      }else{
        return false;
      }

    }

    public function consultarPorCorreo($correo_usu){
      $this->db->where('correo_usu',$correo_usu);
      $usuario=$this->db->get('usuario');
      if ($usuario->num_rows()>0) {
        return $usuario->row();
      }else{
        return false;
      }
    }

  }//cierre de la clase Usuario
 ?>
