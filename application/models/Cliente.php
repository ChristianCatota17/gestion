<?php
  class Cliente extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    public function insertar($datos){
      return $this->db->insert('cliente',$datos);
    }

    public function consultarTodos(){
      $listadoClientes=$this->db->get('cliente');
      if ($listadoClientes->num_rows()>0) {
        return $listadoClientes;
      }else{
        return false;
      }
    }

    public function eliminar($id_cli){
      $this->db->where("id_cli",$id_cli);
      return $this->db->delete("cliente");
    }

    public function actualizar($id_cli,$datos){
      $this->db->where('id_cli',$id_cli);
      return $this->db->update('cliente',$datos);
    }

    public function consultarPorId($id_cli){
      $this->db->where('id_cli',$id_cli);
      $cliente=$this->db->get('cliente');
      if ($cliente->num_rows()>0) {
        return $cliente->row();
      }else{
        return false;
      }
    }


  }//cierre de la clase cliente
 ?>
