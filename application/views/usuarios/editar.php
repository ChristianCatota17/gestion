<br><br>
<center>
<h1>EDITAR USUARIO</h1>
</center>
<br><br>

<div class="row">
  <div class="col-md-1"></div>
  <div class="col-md-10">
<form class="" enctype="multipart/form-data" action="<?php echo site_url(); ?>/usuarios/procesarActualizacion" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id_usu" id="id_usu" value="<?php echo $usuario->id_usu; ?>">

  <label for="" style="color:black;">NOMBRE: </label><br>
  <input class="form-control" type="text" name="nombre_usu" id="nombre_usu" value="<?php echo $usuario->nombre_usu; ?>" placeholder="Por favor ingrese la nombre"><br>
  <label for="" style="color:black;">APELLIDO: </label><br>
  <input class="form-control" type="text" name="apellido_usu" id="apellido_usu" value="<?php echo $usuario->apellido_usu; ?>" placeholder="Por favor ingrese la apellido"><br>

  <label for="" style="color:black;">EMAIL: </label><br>
  <input class="form-control" type="email" name="email_usu" id="email_usu" value="<?php echo $usuario->email_usu; ?>" placeholder="Por favor ingrese el email"><br>
  <label for="" style="color:black;">CONTRASEÑA: </label><br>
  <input class="form-control" type="text" name="password_usu" id="password_usu" value="<?php echo $usuario->password_usu; ?>" placeholder="Por favor ingrese la contraseña"><br>

  <label for="" style="color:black;">PERFIL: </label><br>
  <select class="form-control" name="perfil_usu" id="perfil_usu" style="color:black;">
    <option value="">Seleccione una opción</option>
    <option value="ADMINISTRADOR">ADMINISTRADOR</option>
    <option value="CLIENTE">CLIENTE</option>
  </select>
  <br>
  <label for="" style="color:black;">CORREO: </label><br>
  <input class="form-control" type="email" name="correo_usu" id="correo_usu" value="<?php echo $usuario->correo_usu; ?>" placeholder="Por favor ingrese el correo"><br>

  <label for="">FOTOGRAFIA</label>
    <input type="file" name="foto_usua" accept="image/*" id="foto_usua" value="<?php echo $usuario->foto_usu; ?>">
    <br>
  <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
  &nbsp;&nbsp;&nbsp;

  <a href="<?php echo site_url(); ?>/usuarios/index" class="btn btn-warning">CANCELAR</a>
</form>
</div>
<div class="col-md-1"></div>
</div>

<script type="text/javascript">

  $("#perfil_usu").val("<?php echo $usuario->perfil_usu; ?>")
</script>
<script type="text/javascript">
      $("#foto_usua").fileinput({
        allowedFileExtensions:["jpeg","jpg","png"],
        dropZoneEnabled:true,
        language:"es",
        initialPreview: ["<img src='<?php echo base_url(); ?>/uploads/usuarios/<?php echo $usuario->foto_usu; ?>' class='file-preview-image' alt='' title=''>",]

      });
</script>
