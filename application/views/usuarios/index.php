<div class="row">
  <div class="col-md-12">
    <h1 class="text-center">GESTION DE USUARIOS</h1>
    <center>
      <a class="btn btn-primary" href="<?php echo site_url(); ?>/usuarios/nuevo">Agregar nuevo</a>
      <br><br><br>
    </center>

    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="col-md-10">
        <?php if ($listadoUsuarios): ?>

          <table class="table table-bordered table-hover table-striped"  id="tbl-usu">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">FOTO</th>
                <th class="text-center">NOMBRE</th>
                <th class="text-center">APELLIDO</th>
                <th class="text-center">EMAIL</th>
                <th class="text-center">CONTRASEÑA</th>
                <th class="text-center">PERFIL</th>

                <th class="text-center">OPCIONES</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listadoUsuarios->result() as $filaTemporal): ?>
                <tr>
                  <td class="text-center">
                    <?php echo $filaTemporal->id_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php if ($filaTemporal->foto_usu!=""): ?>
                      <img src="<?php echo base_url(); ?>/uploads/usuarios/<?php echo $filaTemporal->foto_usu; ?>"
                      height="100px"
                      width="100px"
                      alt="">
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->nombre_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->apellido_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->email_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->password_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->perfil_usu; ?>
                  </td>


                  <td class="text-center">
                    <a href="<?php echo site_url(); ?>/usuarios/editar/<?php echo $filaTemporal->id_usu; ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a>
                    <a href="javascript:void(0)"
                    onclick="confirmarEliminacion('<?php echo $filaTemporal->id_usu; ?>')" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                  </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>

        <?php else: ?>
          <div class="alert alert-danger">
            <h3>No de encontraron usuarios</h3>
          </div>
        <?php endif; ?>
      </div>
      <div class="col-md-1">

      </div>
    </div>




  </div>
</div>
<script type="text/javascript">


$('#tbl-usu').DataTable( {
  responsive: true,
  autoWidth: false,
 dom: 'Bfrtip',
 buttons: [
     'copy', 'excel', 'pdf'
 ],
 language: {
            url: "https://cdn.datatables.net/plug-ins/1.12.1/i18n/es-MX.json"
        }



} );
</script>

<script type="text/javascript">
    function confirmarEliminacion(id_usu){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el usuario de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/usuarios/procesarEliminacion/"+id_usu;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>
