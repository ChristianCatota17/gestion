<br><br>
<center>
<h1>EDITAR CLIENTE</h1>
</center>
<br><br>

<div class="row">
  <div class="col-md-1"></div>
  <div class="col-md-10">
<form class="" action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post" enctype="multipart/form-data">
  <input type="hidden" name="id_cli" id="id_cli" value="<?php echo $cliente->id_cli; ?>">

  <label for="" style="color:black;">NOMBRE: </label><br>
  <input class="form-control" type="text" name="nombre_cli" id="nombre_cli" value="<?php echo $cliente->nombre_cli; ?>" placeholder="Por favor ingrese la nombre"><br>
  <label for="" style="color:black;">APELLIDO: </label><br>
  <input class="form-control" type="text" name="apellido_cli" id="apellido_cli" value="<?php echo $cliente->apellido_cli; ?>" placeholder="Por favor ingrese la apellido"><br>

  <label for="" style="color:black;">EMAIL: </label><br>
  <input class="form-control" type="email" name="email_cli" id="email_cli" value="<?php echo $cliente->email_cli; ?>" placeholder="Por favor ingrese el email"><br>
  <label for="" style="color:black;">CONTRASEÑA: </label><br>
  <input class="form-control" type="text" name="password_cli" id="password_cli" value="<?php echo $cliente->password_cli; ?>" placeholder="Por favor ingrese la contraseña"><br>
  <label for="" style="color:black;">DIRECCION: </label><br>
  <input class="form-control" type="text" name="direccion_cli" id="direccion_cli" value="<?php echo $cliente->direccion_cli; ?>" placeholder="Por favor ingrese la direccion"><br>


  <br>
  <button type="submit" name="button" class="btn btn-primary">GUARDAR</button>
  &nbsp;&nbsp;&nbsp;

  <a href="<?php echo site_url(); ?>/clientes/index" class="btn btn-warning">CANCELAR</a>
</form>
</div>
<div class="col-md-1"></div>
</div>
