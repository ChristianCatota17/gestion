<div class="row">
  <div class="col-md-12">
    <h1 class="text-center">GESTION DE CLIENTES</h1>
    <center>
      <a class="btn btn-primary" href="<?php echo site_url(); ?>/clientes/nuevo">Agregar nuevo</a>
      <br><br><br>
    </center>

    <div class="row">
      <div class="col-md-1">

      </div>
      <div class="col-md-10">
        <?php if ($listadoClientes): ?>

          <table class="table table-bordered table-hover table-striped">
            <thead>
              <tr>
                <th class="text-center">ID</th>
                <th class="text-center">NOMBRE</th>
                <th class="text-center">APELLIDO</th>
                <th class="text-center">EMAIL</th>
                <th class="text-center">CONTRASEÑA</th>
                <th class="text-center">DIRECCION</th>
                <th class="text-center">OPCIONES</th>
              </tr>
            </thead>
            <tfoot>
              <?php foreach ($listadoClientes->result() as $filaTemporal): ?>
                <tr>
                  <td class="text-center">
                    <?php echo $filaTemporal->id_cli; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->nombre_cli; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->apellido_cli; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->email_cli; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->password_cli; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $filaTemporal->direccion_cli; ?>
                  </td>

                  <td class="text-center">
                    <a href="<?php echo site_url(); ?>/clientes/editar/<?php echo $filaTemporal->id_cli; ?>" class="btn btn-warning"><i class="fa fa-pen"></i></a>
                    <a onclick="return confirm('¿Esta seguro de eliminar?')" href="<?php echo site_url(); ?>/clientes/procesarEliminacion/<?php echo $filaTemporal->id_cli; ?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                  </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tfoot>
          </table>

        <?php else: ?>
          <div class="alert alert-danger">
            <h3>No de encontraron clientes</h3>
          </div>
        <?php endif; ?>
      </div>
      <div class="col-md-1">

      </div>
    </div>




  </div>
</div>
