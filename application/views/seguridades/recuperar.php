<!DOCTYPE html>
<html lang="es">
<head>
	<title>Iniciar sesión</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/main.css">
</head>
<body class="cover" style="background-image: url(<?php echo base_url(); ?>/assets/assets/img/loginFont.jpg);">

	<form autocomplete="on" class="full-box logInForm" action="<?php echo site_url("seguridades/recuperarPassword"); ?>" method="post">
		<p class="text-center text-muted text-uppercase">Recuperar Contraseña</p>
		<div class="form-group label-floating">
		<label for="" class="control-label">E-mail</label><br>
		<input class="form-control" name="email" type="email" value="" placeholder="Ingrese su email" required><br>
		<p class="help-block">Escribe tú E-mail</p>
		</div>
		<button class="btn" type="submit" name="button">Recuperar ahora.</button>
<a href="<?php echo site_url(); ?>/usuarios/index" class="btn btn-warning">CANCELAR</a>
	</form>





	<!--====== Scripts -->
	<script src="<?php echo base_url(); ?>/assets/js/jquery-3.1.1.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/material.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/ripples.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/sweetalert2.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
	<script>
		$.material.init();
	</script>
</body>
</html>

<?php if ($this->session->flashdata("error")): ?>
<script type="text/javascript">
    alert("<?php echo $this->session->flashdata("error"); ?> ");
</script>
<?php endif; ?>

<?php if ($this->session->flashdata("confirmacion")): ?>
<script type="text/javascript">
iziToast.success({
    title: 'CONFIRMACION',
    message: '<?php echo $this->session->flashdata("confirmacion"); ?>',
    position: 'topRight',
});

</script>
<?php endif; ?>
