<!DOCTYPE html>
<html lang="es">
<head>
	<title>Iniciar sesión</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/main.css">

  <!-- importacion de librerias -->
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.css" integrity="sha512-DIW4FkYTOxjCqRt7oS9BFO+nVOwDL4bzukDyDtMO7crjUZhwpyrWBFroq+IqRe6VnJkTpRAS6nhDvf0w+wHmxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- importacion del jquery validation -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/additional-methods.min.js" integrity="sha512-XJiEiB5jruAcBaVcXyaXtApKjtNie4aCBZ5nnFDIEFrhGIAvitoqQD6xd9ayp5mLODaCeaXfqQMeVs1ZfhKjRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
  <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jq-3.6.0/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-html5-2.2.3/b-print-2.2.3/datatables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/additional-methods.min.js" integrity="sha512-XJiEiB5jruAcBaVcXyaXtApKjtNie4aCBZ5nnFDIEFrhGIAvitoqQD6xd9ayp5mLODaCeaXfqQMeVs1ZfhKjRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

  <script type="text/javascript">
  jQuery.validator.addMethod("letras", function(value, element) {
    //return this.optional(element) || /^[a-z]+$/i.test(value);
    return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñó]*$/.test(value);

  }, "Este campo solo acepta letras");
  </script>

  <!--Importacion de fileinput.js -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/fileinput.min.js" integrity="sha512-C9i+UD9eIMt4Ufev7lkMzz1r7OV8hbAoklKepJW0X6nwu8+ZNV9lXceWAx7pU1RmksTb1VmaLDaopCsJFWSsKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/css/fileinput.min.css" integrity="sha512-XHMymTWTeqMm/7VZghZ2qYTdoJyQxdsauxI4dTaBLJa8d1yKC/wxUXh6lB41Mqj88cPKdr1cn10SCemyLcK76A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/locales/es.min.js" integrity="sha512-q2lXTQuccVsDwaOpJNHbGDL2c5DEK706u1MCjKuGAG4zz+q1Sja3l2RuymU3ySE6RfmTYZ/V4wY5Ol71sRvvWA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


</head>
<body class="cover" style="background-image: url(<?php echo base_url(); ?>/assets/assets/img/loginFont.jpg);">
	<form action="<?php echo site_url(); ?>/seguridades/guardarUsuario" method="post" autocomplete="on" enctype="multipart/form-data" id="frm_nuevo_usu" class="full-box logInForm">

    <div class="form-group label-floating">
		  <label class="control-label">NOMBRE</label>
		  <input class="form-control" name="nombre_usu" id="nombre_usu" type="text" style="color:white;" required>
		  <p class="help-block">Escribe tu nombre</p>
		</div>
    <div class="form-group label-floating">
		  <label class="control-label">APELLIDO</label>
		  <input class="form-control" name="apellido_usu" id="apellido_usu" type="text" style="color:white;" required>
		  <p class="help-block">Escribe tu apellido</p>
		</div>
    <div class="form-group label-floating">
		  <label class="control-label">E-MAIL</label>
		  <input class="form-control" type="email" name="email_usu" id="email_usu" style="color:white;" required>
		  <p class="help-block">Escribe tu e-mail</p>
		</div>

		  <input class="form-control" type="hidden" name="password_usu" id="password_usu" value="" style="color:white;">
		  <input class="form-control" type="hidden" name="perfil_usu" id="perfil_usu" value="CLIENTE" style="color:white;">



    <!-- <div class="form-group label-floating">
		  <label class="control-label">CORREO</label>
		  <input class="form-control" type="email" name="correo_usu" id="correo_usu" style="color:white;">
		  <p class="help-block">Escribe tu correo</p>
		</div> -->


    <!-- <label class="control-label" for="">FOTOGRAFIA</label>
      <input type="file" name="foto_usu" accept="image/*" id="foto_usu" value=""> -->

     <br>
    <button type="submit" name="button" class="btn btn-raised btn-danger">GUARDAR</button>
    &nbsp;&nbsp;&nbsp;

    <a href="<?php echo site_url(); ?>/usuarios/index" class="btn btn-warning">CANCELAR</a>




	</form>



	<!--====== Scripts -->
	<script src="<?php echo base_url(); ?>/assets/js/jquery-3.1.1.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/material.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/ripples.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/sweetalert2.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
	<script>
		$.material.init();
	</script>
</body>
</html>
<script type="text/javascript">
      $("#foto_usu").fileinput({
        allowedFileExtensions:["jpeg","jpg","png"],
        dropZoneEnabled:true,
        language:"es"

      });
</script>
<?php if ($this->session->flashdata("error")): ?>
<script type="text/javascript">
    alert("<?php echo $this->session->flashdata("error"); ?> ");
</script>
<?php endif; ?>

<?php if ($this->session->flashdata("confirmacion")): ?>
<script type="text/javascript">
iziToast.success({
    title: 'CONFIRMACION',
    message: '<?php echo $this->session->flashdata("confirmacion"); ?>',
    position: 'topRight',
});

</script>
<?php endif; ?>
