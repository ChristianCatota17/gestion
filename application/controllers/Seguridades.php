<?php
      class Seguridades extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");
        }

        public function formularioLogin(){

          $this->load->view("seguridades/formularioLogin");

        }

        public function recuperar(){

          $this->load->view("seguridades/recuperar");

        }
        public function registrar(){

          $this->load->view("seguridades/registrar");

        }
        //funcion que valida las credenciales ingresadas
        public function validarAcceso(){
          $email_usu=$this->input->post("email_usu");
          $password_usu=$this->input->post("password_usu");
          $usuario=$this->usuario->buscarUsuarioPorEmailPassword($email_usu,$password_usu);
          if ($usuario) {
            // cuando el email y contraseña son correctas
            if ($usuario->estado_usu>0) {//validando estado
              // creando la variable de sesion con el nombre c0nectadoUTC
              $this->session->set_userdata("c0nectadoUTC",$usuario);
              $this->session->set_flashdata("bienvenido","Saludos, bienvenido al sistema");
              redirect("inicios/index");//la primera vista que vera el usuario
            } else {
              $this->session->set_flashdata("error","Usuario bloqueado");
              redirect("seguridades/formularioLogin");
            }

          }else {//cuando no existe
            $this->session->set_flashdata("error","Email o contraseña incorrectos");
            redirect("seguridades/formularioLogin");
          }
        }

        public function cerrarSesion(){

          $this->session->sess_destroy();//matando la sesiones
          redirect("seguridades/formularioLogin");

        }



        // public function pruebaEmail(){
        //
        //   enviarEmail("cristian.catota2693@utc.edu.ec","PRUEBA","<h1>Cristian Catota</h1><i>Septimo</i>");
        //
        // }

        public function recuperarPassword(){
          $email=$this->input->post("email");
          $usuario=$this->usuario->obtenerPorEmail($email);
          if($usuario){
          $password_aleatorio=rand(11111,999999);
          $emailDestinatario=$email;        
          $asunto="RECUPERAR PASSWORD";
          $contenido="Su contraseña temporal es: <b>$password_aleatorio</b>";
          enviarEmail($emailDestinatario,$asunto,$contenido);
          $data=array("password_usu"=>$password_aleatorio);
          $this->usuario->actualizar($data,$usuario->id_usu);
          $this->session->set_flashdata("confirmacion","Hemos enviado una clave temporal a su correo electronico, por favor cambielo lo mas pronto posible.");

        }else{
          $this->session->set_flashdata("error","El email ingresado no existe");
        }
        redirect("seguridades/formularioLogin");
        }


        public function guardarUsuario(){
            $password_aleatorio=rand(11111,999999);
            $asunto="PASSWORD";
            $contenido="Su contraseña temporal es: <b>$password_aleatorio</b>";
            $emailDestinatario=$this->input->post("email_usu");
            enviarEmail($emailDestinatario,$asunto,$contenido);
            $datosNuevoUsuario=array(
                "nombre_usu"=>$this->input->post("nombre_usu"),
                "apellido_usu"=>$this->input->post("apellido_usu"),
                "email_usu"=>$this->input->post("email_usu"),
                "password_usu"=>$password_aleatorio,
                "perfil_usu"=>$this->input->post("perfil_usu"),
                "correo_usu"=>$this->input->post("correo_usu")
            );



            if($this->usuario->insertar($datosNuevoUsuario)){
                $this->session->set_flashdata("confirmacion","Nuevo usuario generado. Por favor verifique la contraseña en el correo para ingresar al sistema.");
            }else{
            }
            redirect("seguridades/formularioLogin");
        }

      }//cierre de la clase
?>
