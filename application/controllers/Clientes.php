<?php

class Clientes extends CI_Controller{
  public function __construct(){
      parent::__construct();
      $this->load->model("cliente");
      if ($this->session->userdata("c0nectadoUTC")) {
        // si esta conectado
        if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR")
        {
          // SI ES ADMINISTRADOR
        } elseif ($this->session->userdata("c0nectadoUTC")->perfil_usu=="CLIENTE") {
          // code...
        }else {
          redirect("/");
        }
      } else {
        redirect("seguridades/formularioLogin");
      }
    }

    public function index(){
      $data["listadoClientes"]=$this->cliente->consultarTodos();
      $this->load->view("header");
      $this->load->view("clientes/index",$data);
      $this->load->view("footer");
    }
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("clientes/nuevo");
      $this->load->view("footer");
    }

    public function guardarCliente(){
        $datosNuevoCliente=array(
            "nombre_cli"=>$this->input->post("nombre_cli"),
            "apellido_cli"=>$this->input->post("apellido_cli"),
            "email_cli"=>$this->input->post("email_cli"),
            "password_cli"=>$this->input->post("password_cli"),
            "direccion_cli"=>$this->input->post("direccion_cli")
        );
        if($this->cliente->insertar($datosNuevoCliente)){
            $this->session->set_flashdata("confirmacion","Cliente insertado exitosamente.");
        }else{
        }
        redirect("clientes/index");
    }

    public function procesarEliminacion($id_cli){
      if ($this->cliente->eliminar($id_cli)) {
        redirect("clientes/index");
      }else {
        echo "no eliminado";
      }

    }

    public function editar($id_cli){
      $data["cliente"]=$this->cliente->consultarPorId($id_cli);
      $this->load->view("header");
      $this->load->view("clientes/editar",$data);
      $this->load->view("footer");
    }

    public function procesarActualizacion(){
      $id_cli=$this->input->post("id_cli");
      $datosClienteEditado=array(
        "nombre_cli"=>$this->input->post("nombre_cli"),
        "apellido_cli"=>$this->input->post("apellido_cli"),
        "email_cli"=>$this->input->post("email_cli"),
        "password_cli"=>$this->input->post("password_cli"),
        "direccion_cli"=>$this->input->post("direccion_cli")
    );
      if($this->cliente->actualizar($id_cli,$datosClienteEditado)){
          redirect("clientes/index");
      }else{
        echo "error al editar";
      }
    }

}//cierre de la clase
?>
