<?php

class Usuarios extends CI_Controller{
  public function __construct(){
      parent::__construct();
      $this->load->model("usuario");
      if ($this->session->userdata("c0nectadoUTC")) {
        // si esta conectado
        if ($this->session->userdata("c0nectadoUTC")->perfil_usu=="ADMINISTRADOR")
        {
          // SI ES ADMINISTRADOR
        } else {
          redirect("/");
        }
      } else {
        redirect("seguridades/formularioLogin");
      }
    }

    public function index(){
      $data["listadoUsuarios"]=$this->usuario->consultarTodos();
      $this->load->view("header");
      $this->load->view("usuarios/index",$data);
      $this->load->view("footer");
    }
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("usuarios/nuevo");
      $this->load->view("footer");
    }

    public function guardarUsuario(){
        $datosNuevoUsuario=array(
            "nombre_usu"=>$this->input->post("nombre_usu"),
            "apellido_usu"=>$this->input->post("apellido_usu"),
            "email_usu"=>$this->input->post("email_usu"),
            "password_usu"=>$this->input->post("password_usu"),
            "perfil_usu"=>$this->input->post("perfil_usu"),
            "correo_usu"=>$this->input->post("correo_usu"),
            "foto_usu"=>$dataSubida["file_name"]
        );

        //Logica de Negocio necesaria para subir la FOTOGRAFIA del usuario
          $this->load->library("upload");//carga de la libreria de subida de archivos
          $nombreTemporal="foto_usuario_".time()."_".rand(1,5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/usuarios/';
          $config["allowed_types"]="jpeg|jpg|png";
          $config["max_size"]=2*1024; //2MB
          $this->upload->initialize($config);
          //codigo para subir el archivo y guardar el nombre en la BDD
          if($this->upload->do_upload("foto_usu")){
            $dataSubida=$this->upload->data();
            $datosNuevoUsuario["foto_usu"]=$dataSubida["file_name"];
          }

        if($this->usuario->insertar($datosNuevoUsuario)){
            $this->session->set_flashdata("confirmacion","Usuario insertado exitosamente.");
        }else{
        }
        redirect("usuarios/index");
    }

    public function procesarEliminacion($id_usu){
      if ($this->usuario->eliminar($id_usu)) {
        $this->session->set_flashdata("confirmacion","Usuario eliminado exitosamente.");
      }else {
        $this->session->set_flashdata("confirmacion","Usuario no eliminado.");
      }
      redirect("usuarios/index");

    }

    public function editar($id_usu){
      $data["usuario"]=$this->usuario->consultarPorId($id_usu);
      $this->load->view("header");
      $this->load->view("usuarios/editar",$data);
      $this->load->view("footer");
    }

    public function procesarActualizacion(){
      $id_usu=$this->input->post("id_usu");

      $foto_usu=$this->input->post('foto_usu');
      //logica de negocio necesaria para subir la fotografia del usuarios
      $this->load->library("upload"); //Carga de la libreria de subida de archivos

          if ($foto_usu=="") {
            // code...
            $nombreTemporal="foto_usuario_".time()."_".rand(1,5000); //creando un nombre aleatorio
            $config['file_name']=$nombre_aleatorio; //asignano el nombre al archivo subido
            $foto_usu=$nombre_aleatorio;
          }else {
            unlink(  APPPATH.'../uploads/usuarios/'.$foto_usu);// si es true, llama la función
            $config["file_name"]=$foto_usu; //asignano el nombre al archivo subido
          }
      $config["upload_path"]=APPPATH.'../uploads/usuarios/'; //direccion de la carpeta para el guardado de las imgenes
      $config["allowed_types"]="jpeg|jpg|png"; //aqui va el formato de los archivos que deseamos cargar en este caso son imagenes si deseamos subir documento cambiamos
      $config["max_size"]=2*1024; //Tamaño maximo del archivo que deseamos que se suba en este cas esta 2MB
      $this->upload->initialize($config);

      if($this->upload->do_upload("foto_usua")){
        $dataSubida=$this->upload->data();
        $datosNuevoUsuario["foto_usua"]=$dataSubida["file_name"];
      }


      $datosUsuarioEditado=array(
        "nombre_usu"=>$this->input->post("nombre_usu"),
        "apellido_usu"=>$this->input->post("apellido_usu"),
        "email_usu"=>$this->input->post("email_usu"),
        "password_usu"=>$this->input->post("password_usu"),
        "perfil_usu"=>$this->input->post("perfil_usu"),
        "correo_usu"=>$this->input->post("correo_usu"),
        "foto_usu"=>$dataSubida["file_name"]
    );
      if($this->usuario->actualizar($id_usu,$datosUsuarioEditado)){
          redirect("usuarios/index");
      }else{
        echo "error al editar";
      }
    }

}//cierre de la clase
?>
